# **APO Semestral work**
### Michal Lenc, Martin Maxa
___
![Block schema](documentation/img/Basic_division.png "Block schema")

### _Brief description_
___
**Main menu**
- can be displayed as a text or graphic, user can choose by using standard input

**Game of Life**
- the implementation of John Conway's Game of Life

**Pong**
- classical table tennis game for two players which is control by using standard input

**Image convolution** 
- based on HW02, user can change kernel matrix by using standard input

**LEDs control** 
- user can change RGB LEDs colour using standard input or knobs, effects menu can play various colourful effects.

### _Description of main functions_
___

#### Game of Life
![Game of Life](documentation/img/GoF.png "game of life implementation")

Implementation of well knows cellular automat, which was devised by British mathematician J. H. Conway. It is a zero-player game with specific rulers that provides “life” in this program.

**There are these two basic rules that we applicated as basic setup:**
1. Any live cell with two or three neighbours will survive.
2. Any dead cell with exactly three neighbours will become reborn. 
In other cases, cell will die in the next generation.

User can this setting always change by ‘s’ on a keyboard and after that user could reset this setting by pressing ‘d’ key. Also, there is a function that provides reset the grid. Users can use it by pressing ‘r’.

 **In Game of Life are these main functions:**
int game_of_life(void)
- main function for Game of Life which is called from main.c, the function inits and starts POSIX threads

void *inputThread(void *arg)
- input thread for reading characters coming from standard inputvoid *drawThread(void *arg)–thread function for drawing game of life generations to LCD display

void compute()
- function for computing generations of game of life, the function calls int neighbors(int x, int y) to calculate the number of neighbours for each cell

#### Image convolution
![Image convolution](documentation/img/Convolution.png "Image convolution implementation")

This subprogram is based on homework 2 in the summer semester. Convolution is image processing that can be used for blurring, edge detection, sharpening, and more. It is a basic program in all graphic editors.
Our implementation of this program read the image from file and process the convolution. After that, the result is displayed on display. Users must set the values of the matrix by standard input always before the convolution.

**In Image convolution are these main functions:**
void imageMain(void)
- main function for image subprogram called from main.c, the user set the proper image and the function then calls

_Bool konvoluceSetup(void)
- set the parameters, compute convolution and print the following image to LCD display

_Bool readImage(const char *fname)
- reads image from file_Bool konvolutionSetup(void)–read matrix values from user

_Bool konvolution(input matrix)
- process convolution

#### LED controller
![LED controller](documentation/img/Leds.png "LED controller implemenation")

Leds.c is an RGB LED controller with several functions. There are these main functions:
1. Control RGB colour by keyboard
2. Control RGB colour by knobs
3. Predefined RGB colour effects

In the first mode, users can set the colour by a keyboard. For example “255 + 0 + 0” program will set the RGB at rgb(255,0,0) which is red.
There is a very intuitive LED menu that helps users to have a better experience. The menu shows users running applications, and the colour of the LED is displayed on display in the right bottom corner at the screen.

**In Image convolution are these main functions:**
void ledsMain(void)
- main function for LEDcalled from main.c, user sets whether he wants to controlLEDfrom standard input or by using knobs or whether effects are to be played

void keyboardLed(void)
- function for controlling LEDfrom keyboard, the function calls void changeColor(uint32_t colorRGB) to change the colours of both RGB LEDand the led line

void knobsLed(void)
- function for controlling LEDby using knobs,the function calls void changeColor(uint32_t colorRGB) to change the colours of both RGB LED and the LED line

void effectsMain(void)
- displays available effects and then calls proper function to make these effects

#### Pong game
![Pong](documentation/img/Pong.png "Implementation of pong game")

Implementation one of the oldest video games that helped to established video game industry. This game has very intuitive control and it can play it anybody. 
**In pong are these main functions:**
int pong()
- main function for pong game called from main.c, the function inits and starts POSIX threads and displays the results after the end of game

void *input_thread(void* arg)
- input thread for reading characters coming from standard input•void *control_thread(void* arg)–thread used for controlling both paddles

void *ball_thread(void* arg)
- thread used for moving the ball and controlling whether the player scored a point

void *led_thread(void* arg)
- thread used for changing led line for visual effect






/*******************************************************************

  Main program including menu and leading to other subprograms

  Git repository: https://gitlab.fel.cvut.cz/maxamart/apo_semestralka

  Martin Maxa & Michal Lenc, public domain

 *******************************************************************/

#define _POSIX_C_SOURCE 200112L
//include standard libraries
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
//include other files
#include "mzapo/mzapo_parlcd.h"
#include "mzapo/mzapo_phys.h"
#include "mzapo/mzapo_regs.h"
#include "gameOfLife/game_of_life.h"
#include "pong/pong.h"
#include "image/image.h"
#include "utils.h"
#include "lcdControl/lcdControl.h"
#include "leds/leds.h"

/*-----------------Global variables-------------------------------------------*/

unsigned char *mem_base;
unsigned char *parlcd_base;

/*-----------------Main-------------------------------------------------------*/
int main(int argc, char *argv[])
{
	printf("Hello world\n");

	mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
	parlcd_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);

	//parlcd_hx8357_init(parlcd_base);

	if (mem_base == NULL || parlcd_base == NULL)
	{
		fprintf(stderr, "ERROR: Cannot setup memor mapping!\n");
		exit(-1);
	}
	changeBase(parlcd_base);
	changeMemBase(mem_base);

	setLCDbackground(0); //set backround to black
	parlcd_write_cmd(parlcd_base, 0x2c);
	write_free_lines_to_parlcd(60);
	write_words_to_parlcd("Welcome", 4); //write Welcome
	write_free_lines_to_parlcd(20);
	write_words_to_parlcd("G-graphic menu", 3);
	write_free_lines_to_parlcd(20);
	write_words_to_parlcd("T-text menu", 3);

	bool display;
	char d;
	call_termios(0);
	info("Press 'G' for graphic display or 'T' for text display");
	info("You can later change the menu by pressing 'M'");
	while (true)
	{
		if (scanf("%c", &d) != 1){ //if error while reading
			error("input char error");
			exit(-1);
		}
		if (d == 'G' || d == 'g'){
			display = true; //if graphic display
			break;
		}
		else if (d == 'T' || d == 't'){ //if text display
			display = false;
			break;
		}
		else {
			error("Wrong character, try again!");
		}
	}
	char c;
	int r = 0;
	bool end = false;
	while (!end)
	{
		if (display)
		{
			if (!create_menu())
			{ //create menu failed
				error("ERROR while creating menu");
			}
		}
		else
		{
			write_menu_to_parlcd();
		}

		printf("Type the folowing characters to select an app.\n");
		printf("\tG = Game of Life\n");
		printf("\tL = Chaning color of leds\n");
		printf("\tI = Image convolution\n");
		printf("\tP = Play pong\n");
		printf("\tQ = Quit\n");

		r = 0;

		if (scanf("%c", &c) != 1)
		{ //&& (c != 'G' || c != 'L')) { //reading input choice
			error("ERROR while reading from stdin");
			return -2;
		}

		switch (c)
		{ //evaluation of choice
		case 'g':
		case 'G':
			r = game_of_life();
			printf("%d\n", r);
			break;
		case 'l':
		case 'L':
			ledsMain();
			break;
		case 'p':
		case 'P':
			info("Starting pong");
			pong();
			break;
		case 'i':
		case 'I':
			imageMain();
			break;
		case 'm':
		case 'M':
			display = !display; //change of the display
			break;
		case 'q':
		case 'Q':
			info("Quit");
			end = true;
			break;
		default:
			error("Wrong choice!");
			break;
		} //end switch
	}	  //end while

	call_termios(1);
	setLCDbackground(0); //set black
	parlcd_write_cmd(parlcd_base, 0x2c);
	write_free_lines_to_parlcd(128);
	write_words_to_parlcd("Goodbye", 4); //print Goodbye message
	printf("Goodbye world\n");

	return EXIT_SUCCESS;
}

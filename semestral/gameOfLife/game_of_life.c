/*******************************************************************
  Subprogram for generating Conway´s Game of Life

  Git repository: https://gitlab.fel.cvut.cz/maxamart/apo_semestralka

  Martin Maxa & Michal Lenc, public domain

 *******************************************************************/

#include "game_of_life.h"
#include "../mzapo/mzapo_parlcd.h"
#include "../mzapo/mzapo_phys.h"
#include "../mzapo/mzapo_regs.h"
#include "../utils.h"
#include "../lcdControl/lcdControl.h"

enum
{
  ERROR_INIT = -1,
  ERROR_START = -2
};

typedef struct data
{
  _Bool quit;
  pthread_cond_t gof_cond;
  pthread_mutex_t gof_mtx;
} data_t;

typedef struct settings
{
  int num_cells_stable;
  int num_cells_alive;
} settings_t;

data_t data = {.quit = false};
settings_t set = {.num_cells_stable = 3, .num_cells_alive = 2};

int game_of_life(void)
{
  data.quit = false;
  if (pthread_mutex_init(&(data.gof_mtx), NULL))
  {
    fprintf(stderr, "ERROR: Could not initialize mutex.\n");
    data.quit = true;
  }
  if (pthread_cond_init(&(data.gof_cond), NULL))
  {
    fprintf(stderr, "ERROR: Could not initialize condvar.\n");
    data.quit = true;
  }
  if (data.quit)
  {
    return ERROR_INIT;
  }

  enum
  {
    INPUT_THREAD,
    DRAW_THREAD,
    NUM_THREADS
  };

  const char *thread_names[] = {"Input", "Drawing"};

  void *(*thd_functions[])(void *) = {
      inputThread,
      drawThread};

  pthread_t threads[NUM_THREADS]; //array of threads

  for (int i = 0; i < NUM_THREADS; ++i)
  { //start the threads
    int check = pthread_create(&threads[i], NULL, thd_functions[i], NULL);
    fprintf(stderr, "INFO: %s thread start: %s\r\n",
            thread_names[i],
            check ? "FAIL" : "=OK=");
    if (check != 0)
    {
      return ERROR_START;
    }
  }
  for (int i = 0; i < NUM_THREADS; ++i)
  { //join the threads
    int check = pthread_join(threads[i], NULL);
    fprintf(stderr, "INFO: %s thread joined: %s\r\n",
            thread_names[i],
            check ? "FAIL" : "=OK=");
  }

  setLCDbackground(0);
  info("Game of Life ending...");
  write_free_lines_to_parlcd(120);
  write_words_to_parlcd("Game of Life ending...", 2);
  fflush(stdin);
  sleep(2);
  return EXIT_SUCCESS;
}

void *inputThread(void *arg)
{
  //data_t *data = (data_t *)arg;
  char c;
  _Bool end = false;

  info("Pres 'q' to quit, 's' to change settings, 'd' to set default setings, 'r' to reset grid");

  while ((c = getchar()) != 'q' && !end)
  {
    switch (c)
    {
    case 'q':
      end = true;
      break;
    case 's':
      settings();
      break;
    case 'r': //reset grid
      generateNewGrid();
      break;
    case 'd': //set default values
      info("Set default setings");
      set.num_cells_stable = 3;
      set.num_cells_alive = 2;
      break;
    default:
      break;
    }
  }
  info("Set quit.");
  pthread_mutex_lock(&(data.gof_mtx));
  data.quit = true;
  pthread_mutex_unlock(&(data.gof_mtx));
  return NULL;
}

void *drawThread(void *arg)
{
  //data_t *data = (data_t *)arg;

  srand(time(0));
  for (int i = 0; i < ROWS; i++)
  {
    for (int j = 0; j < COLS; j++)
    {
      grid[i][j] = rand() % 2;
    }
  }

  unsigned c;
  _Bool end = false;

  while (!end)
  {
    pthread_mutex_lock(&(data.gof_mtx));
    end = data.quit;
    pthread_mutex_unlock(&(data.gof_mtx));

    compute();
    parlcd_write_cmd(getBase(), 0x2c);
    for (int j = 0; j < 320 && !data.quit; j++)
    {
      for (int k = 0; k < 480 && !data.quit; k++)
      {
        if (grid[j][k] == 0)
        {
          c = 0xFFFF;
        }
        else
        {
          c = 0x0;
        }
        parlcd_write_data(getBase(), c);
      }
    }
  }
  return NULL;
}

void compute()
{
  for (int i = 0; i < ROWS; i++)
  {
    for (int j = 0; j < COLS; j++)
    {
      int sum = neighbors(i, j);
      if (sum == set.num_cells_stable)
      {
        next[i][j] = 1;
      }
      else if (sum == set.num_cells_alive)
      {
        next[i][j] = grid[i][j];
      }
      else
      {
        next[i][j] = 0;
      }
    }
  }
  for (int i = 0; i < ROWS; i++)
  {
    for (int j = 0; j < COLS; j++)
    {
      grid[i][j] = next[i][j];
    }
  }
}

int neighbors(int x, int y)
{
  int ret = 0;
  for (int i = -1; i < 2; i++)
  {
    for (int j = -1; j < 2; j++)
    {
      int row = (x + i + ROWS) % ROWS;
      int col = (y + j + COLS) % COLS;
      ret += grid[row][col];
    }
  }
  ret -= grid[x][y];
  return ret;
}

void settings(void)
{
  info("settings");
  call_termios(1);
  event("Choose how many cells should be in suraroundings when a cell come into being");
  if (!scanf("%d", &(set.num_cells_stable)))
  {
    error("reading input");
  }

  event("Choose how many cells must be in suraroundings for a cell to survive");
  if (!scanf("%d", &(set.num_cells_alive)))
  {
    error("reading input");
  }
  call_termios(0);
}

void generateNewGrid(void)
{
  info("reset grid");
  for (int i = 0; i < ROWS; i++)
  {
    for (int j = 0; j < COLS; j++)
    {
      grid[i][j] = rand() % 2;
    }
  }
}

/*******************************************************************
  Subprogram for generating Conway´s Game of Life

  Git repository: https://gitlab.fel.cvut.cz/maxamart/apo_semestralka

  Martin Maxa & Michal Lenc, public domain

 *******************************************************************/

#ifndef GAME_OF_LIFE_H
#define GAME_OF_LIFE_H

#include <pthread.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>


#define ROWS 320
#define COLS 480

int grid[ROWS][COLS];
int next[ROWS][COLS];

void *inputThread(void *arg); //input thread
void *drawThread(void *arg); //drawing threa
void settings(void); //change settings
void generateNewGrid(void); //generate new random grid

void compute(); //compute game of life array
int game_of_life(void); //main func for game of life
int neighbors(int x, int y); //compute number of neighbors

#endif

/*******************************************************************

  Debugging functions, RGB converter

  Git repository: https://gitlab.fel.cvut.cz/maxamart/apo_semestralka

  Martin Maxa & Michal Lenc, public domain

 *******************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>

#include "utils.h"

//colors for terminal
#define KNRM "\x1B[0m"  //normal
#define KRED "\x1B[31m" //red
#define KGRN "\x1B[32m" //greeen
#define KYEL "\x1B[33m" //yellow
#define KBLU "\x1B[34m" //blue
#define KMAG "\x1B[35m" //magenta
#define KCYN "\x1B[36m" //cyan
#define KWHT "\x1B[37m" //white

utilsData_t utilsData = {.parlcd_base = NULL, .mem_base = NULL};

void changeBase(unsigned char *base)
{
    utilsData.parlcd_base = base;
}

unsigned char *getBase(void)
{
    return utilsData.parlcd_base;
}

void changeMemBase(unsigned char *base)
{
    utilsData.mem_base = base;
}

unsigned char *getMemBase(void)
{
    return utilsData.mem_base;
}

void call_termios(int reset)
{
    static struct termios tio, tioOld;
    tcgetattr(STDIN_FILENO, &tio);
    if (reset)
    {
        tcsetattr(STDIN_FILENO, TCSANOW, &tioOld);
    }
    else
    {
        tioOld = tio; //backup
        cfmakeraw(&tio);
        tio.c_oflag |= OPOST;
        tcsetattr(STDIN_FILENO, TCSANOW, &tio);
    }
}

/*-----------------Functions--------------------------------------------------*/
uint32_t colorRGB(unsigned char r, unsigned char g, unsigned char b)
{
    return (r << 16 | g << 8 | b);
}

/*-----------------Functions--------------------------------------------------*/
unsigned short colorRGB565(unsigned char r, unsigned char g, unsigned char b)
{
    unsigned short r_val, g_val, b_val;

    r_val = (r >> 3) << 11;
    g_val = (g >> 2) << 5;
    b_val = b >> 3;

    return r_val | g_val | b_val;
}

void info(const char *str)
{
    fprintf(stderr, "%sINFO: %s%s\n", KMAG, str, KNRM);
}

void event(const char *str)
{
    fprintf(stderr, "%sEVENT: %s%s\n", KGRN, str, KNRM);
}

void debug(const char *str)
{
    fprintf(stderr, "%sDEBUG: %s%s\n", KWHT, str, KNRM);
}

void error(const char *str)
{
    fprintf(stderr, "%sERROR: %s%s\n", KRED, str, KNRM);
}

void warn(const char *str)
{
    fprintf(stderr, "%sWARN: %s%s\n", KYEL, str, KNRM);
}

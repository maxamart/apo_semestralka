/*******************************************************************
  Subprogram for controling LEDs on MZAPO board

  Git repository: https://gitlab.fel.cvut.cz/maxamart/apo_semestralka

  Martin Maxa & Michal Lenc, public domain

 *******************************************************************/

#ifndef _leds_
#define _leds_

#include <stdlib.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>


void ledsMain(void); //main function for leds called from main.c

void effectsMain(void); //main function for choosing effects
void keyboardLed(void); //controlling leds from keyboard
_Bool leds_menu(void); //displays led menu on LCD
_Bool effects_menu(void); //displys effect menu on LCD
void knobsLed(void); //controlling leds by knobs
void updateKnobs(void);
void ledFading(void); //led fading effect
void ledBlinking(void); //led blinking effect
void ledEffect3(void); //effect n. 3
void ledEffect4(void); //effect n. 4
void changeColor(uint32_t colorRGB);
void setSpeed(void); //set speed of effects


#endif

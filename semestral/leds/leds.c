/*******************************************************************
  Subprogram for controling LEDs on MZAPO board

  Git repository: https://gitlab.fel.cvut.cz/maxamart/apo_semestralka

  Martin Maxa & Michal Lenc, public domain

 *******************************************************************/

#include "leds.h"
#include "../utils.h"
#include "../lcdControl/lcdControl.h"

#include "../mzapo/mzapo_parlcd.h"
#include "../mzapo/mzapo_phys.h"
#include "../mzapo/mzapo_regs.h"

typedef struct
{
    uint32_t knobs_value;
    uint8_t red_knob;
    uint8_t green_knob;
    uint8_t blue_knob;
    _Bool red_button;
    _Bool green_button;
    _Bool blue_button;
} knobs_values_t;

typedef struct
{
    int speed;
} ledSettings_t;

knobs_values_t knobs = {.knobs_value = 0, .red_knob = 0, .green_knob = 0, .blue_knob = 0};
ledSettings_t setLed = {.speed = 5};


/*-----------------Functions--------------------------------------------------*/
void ledsMain(void)
{
    leds_menu();
    *(volatile uint32_t *)(getMemBase() + SPILED_REG_KNOBS_8BIT_o) = 0;

    char c;
    _Bool end = false;

    while (!end)
    {
        event("Press B for keyboart settings, K for knobs settings, E for effects, Q for quit");
        fflush(stdin);
        if (scanf("%c", &c) != 1)
        {
            error("reading");
        }
        if (c == 'B' || c == 'b')
        {
            call_termios(1);
            keyboardLed();
            call_termios(0);
        }
        else if (c == 'K' || c == 'k')
        {
            knobsLed();
        }
        else if (c == 'E' || c == 'e')
        {
            effectsMain();
            leds_menu();
        }
        else if (c == 'Q' || c == 'q')
        {
            end = true;
        }
    }
}

void effectsMain(void)
{
    effects_menu();
    char c;

    while (1)
    {
        event("Press 1 - 4 to select effect, Q to quit");
        fflush(stdin);
        if (scanf("%c", &c) != 1)
        {
            error("reading");
        }

        switch (c)
        {
        case '1':
            ledFading();
            changeColor(0);
            break;
        case '2':
            ledBlinking();
            changeColor(0);
            break;
        case '3':
            ledEffect3();
            changeColor(0);
            break;
        case '4':
            ledEffect4();
            changeColor(0);
            break;
        case 'q':
        case 'Q':
            info("Set quit");
            changeColor(0);
            return;
        default:
            break;
        }
    }
}

//func changing color of LEDs until '0+0+0' (quit) is typed
void keyboardLed(void)
{
    create_box(1, 0, 10000);
    write_string("B - keyboard settings", 1, 1, 0);
    screenToLCD();

    event("For changing color type RGB model as 'red+green+blue' for example '255+255+255'");
    event("For end this app write '0+0+0'");

    unsigned char value_R, value_B, value_G;
    uint32_t value; //input values RGB and output value

    while (1)
    {
        if (scanf("%hhi+%hhi+%hhi", &value_R, &value_G, &value_B) != 3)
        {
            error("ERROR: Cannot read RGB values!");
            value_R = value_G = value_B = 0;
        }

        value = colorRGB(value_R, value_G, value_B);

        if (value > 0)
        {
            changeColor(value);
            value = colorRGB565(value_R, value_G, value_B);
            write_string("COLOR:", 2, 3, 0);
            create_box(3, 1, value);
            screenToLCD();

            info("LED colors changed.");
        }
        else
        { //value = 0, user set quit
            create_box(1, 0, 1296);
            write_string("B - keyboard settings", 1, 1, 0);
            create_box(3, 1, 0);
            screenToLCD();
            changeColor(0);
            return;
        }
    } //end while
}

void knobsLed(void)
{
    uint32_t value;
    create_box(1, 1, 10000);
    write_string("K - Knobs settings", 1, 1, 1);
    screenToLCD();

    info("Press any button to quit knobs settings");
    while (1)
    {
        updateKnobs();

        if (knobs.red_button || knobs.green_button || knobs.blue_button)
        { //exit
            info("Button pressed - setting quit");
            create_box(1, 1, 1296);
            create_box(3, 1, 0);
            write_string("K - Knobs settings", 1, 1, 1);
            screenToLCD();
            changeColor(0);
            return;
        }

        value = colorRGB(knobs.red_knob, knobs.green_knob, knobs.blue_knob);

        changeColor(value);

        value = colorRGB565(knobs.red_knob, knobs.green_knob, knobs.blue_knob);
        write_string("COLOR:", 2, 3, 0);
        create_box(3, 1, value);
        screenToLCD();
    }
}

_Bool leds_menu(void)
{
    setLCDbackground(0);

    create_box(1, 0, 1296);
    create_box(1, 1, 1296);
    create_box(2, 0, 1296);
    create_box(2, 1, 5000);
    create_box(3, 1, 0);

    write_string("COLOR:", 2, 3, 0);
    write_string("LEDs MENU", 3, 0, 0);
    write_string("B - keyboard settings", 1, 1, 0);
    write_string("K - Knobs settings", 1, 1, 1);
    write_string("E - Effects", 1, 2, 0);
    write_string("Q - QUIT", 1, 2, 1);

    screenToLCD();

    return true;
}

_Bool effects_menu(void)
{
    setLCDbackground(0);

    create_box(1, 0, 1296);
    create_box(1, 1, 1296);
    create_box(2, 0, 1296);
    create_box(2, 1, 1296);
    create_box(3, 1, 5000);

    write_string("Effects MENU", 3, 0, 0);
    write_string("1 - Fading RGB", 1, 1, 0);
    write_string("2 - Blinking", 1, 1, 1);
    write_string("3 - Fading v2", 1, 2, 0);
    write_string("4 - Police", 1, 2, 1);
    write_string("Q - QUIT", 1, 3, 1);

    screenToLCD();

    return true;
}

void updateKnobs(void)
{
    uint32_t value = *(volatile uint32_t *)(getMemBase() + SPILED_REG_KNOBS_8BIT_o);
    knobs.knobs_value = value;
    knobs.blue_knob = value & 0xFF;
    knobs.green_knob = (value >> 8) & 0xFF;
    knobs.red_knob = (value >> 16) & 0xFF;

    knobs.blue_button = (value >> 24) & 1;
    knobs.green_button = (value >> 25) & 1;
    knobs.red_button = (value >> 26) & 1;
}

void ledFading(void)
{
    create_box(1, 0, 10000);
    write_string("1 - Fading RGB", 1, 1, 0);
    screenToLCD();

    int i;
    _Bool end = false;
    unsigned char r, g, b;
    b = 255;
    r = g = 0;

    setSpeed();

    info("For end press any knob");
    while (!end)
    {
        //blue to red
        for (i = 0; i < 255 && !end; i += 1, ++r, --b)
        {
            updateKnobs();
            changeColor(colorRGB(r, g, b));
            if (knobs.red_button || knobs.green_button || knobs.blue_button)
            {
                end = true;
            }
            usleep((10 - setLed.speed) * 25);
        }
        //red to green
        for (i = 0; i < 255 && !end; i += 1, --r, ++g)
        {
            updateKnobs();
            changeColor(colorRGB(r, g, b));
            if (knobs.red_button || knobs.green_button || knobs.blue_button)
            {
                end = true;
            }
            usleep((10 - setLed.speed) * 25);
        }
        //green to blue
        for (i = 0; i < 255 && !end; i += 1, --g, ++b)
        {
            updateKnobs();
            changeColor(colorRGB(r, g, b));
            if (knobs.red_button || knobs.green_button || knobs.blue_button)
            {
                end = true;
            }
            usleep((10 - setLed.speed) * 25);
        }
    }
    create_box(1, 0, 1296);
    write_string("1 - Fading RGB", 1, 1, 0);
    screenToLCD();
}

void ledBlinking(void)
{
    create_box(1, 1, 10000);
    write_string("2 - Blinking", 1, 1, 1);
    screenToLCD();

    int i;
    _Bool end = false;
    unsigned char r, g, b;
    b = r = g = 0;

    setSpeed();

    info("For end press any knob");
    while (!end)
    {
        for (i = 0; i < 255 && !end; i += 1, ++b, ++g, ++r)
        {
            updateKnobs();
            changeColor(colorRGB(r, g, b));
            if (knobs.red_button || knobs.green_button || knobs.blue_button)
            {
                end = true;
            }
            usleep((10 - setLed.speed) * 20);
        }
        usleep(500);
        for (i = 0; i < 255 && !end; i += 1, --b, --g, --r)
        {
            updateKnobs();
            changeColor(colorRGB(r, g, b));
            if (knobs.red_button || knobs.green_button || knobs.blue_button)
            {
                end = true;
            }
            usleep((10 - setLed.speed) * 20);
        }
        usleep(500);
    }
    create_box(1, 1, 1296);
    write_string("2 - Blinking", 1, 1, 1);
    screenToLCD();
}

void ledEffect3(void)
{
    create_box(2, 0, 10000);
    write_string("3 - Fading v2", 1, 2, 0);
    screenToLCD();

    int i;
    _Bool end = false;
    unsigned char r, g, b;
    b = r = g = 0;

    setSpeed();

    info("For end press any knob");
    while (!end)
    {
        for (i = 0; i < 250 && !end; i += 1)
        {
            updateKnobs();
            changeColor(colorRGB(r, g, b));
            if (knobs.red_button || knobs.green_button || knobs.blue_button)
            {
                end = true;
            }
            if (i % 50)
            {
                b = i;
            }
            usleep((10 - setLed.speed) * 40);
        }
        usleep(500);

        for (i = 0; i < 250 && !end; i += 1)
        {
            updateKnobs();
            changeColor(colorRGB(r, g, b));
            if (knobs.red_button || knobs.green_button || knobs.blue_button)
            {
                end = true;
            }
            if (i % 50)
            {
                b = 0;
                g = i;
            }
            usleep((10 - setLed.speed) * 40);
        }
        usleep(500);
        for (i = 0; i < 250 && !end; i += 1)
        {
            updateKnobs();
            changeColor(colorRGB(r, g, b));
            if (knobs.red_button || knobs.green_button || knobs.blue_button)
            {
                end = true;
            }
            if (i % 50)
            {
                r = i;
                g = 0;
            }
            usleep((10 - setLed.speed) * 40);
        }
        usleep(500);
    }
    create_box(2, 0, 1296);
    write_string("3 - Fading v2", 1, 2, 0);
    screenToLCD();
}

void ledEffect4(void)
{
    create_box(2, 1, 10000);
    write_string("4 - Police", 1, 2, 1);
    screenToLCD();

    int i;
    _Bool end = false;
    unsigned char r, g, b;
    r = g = 0;
    b = 255;

    setSpeed();

    info("For end press any knob");
    while (!end)
    {
        for (i = 0; i < 250 && !end; i += 1, ++r, --b)
        {
            updateKnobs();
            changeColor(colorRGB(r, g, b));
            if (knobs.red_button || knobs.green_button || knobs.blue_button)
            {
                end = true;
            }
            usleep((10 - setLed.speed) * 60);
        }
    }
    create_box(2, 1, 1296);
    write_string("4 - Police", 1, 2, 1);
    screenToLCD();
}

void changeColor(uint32_t colorRGB)
{
    *(volatile uint32_t *)(getMemBase() + SPILED_REG_LED_RGB1_o) = colorRGB;
    *(volatile uint32_t *)(getMemBase() + SPILED_REG_LED_RGB2_o) = colorRGB;
    *(volatile uint32_t *)(getMemBase() + SPILED_REG_LED_LINE_o) = colorRGB;
}

void setSpeed(void)
{
    int speed;
    call_termios(1);
    event("Write speed (0-10):");

    if (scanf("%d", &speed) != 1)
    {
        error("reading");
    }

    if (speed < 0 || speed > 10)
    {
        error("Value, it will be set default");
        setLed.speed = 5;
    }
    else
    {
        setLed.speed = speed;
    }
    call_termios(0);
}

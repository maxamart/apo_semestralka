/*******************************************************************

  Debugging functions, RGB converter

  Git repository: https://gitlab.fel.cvut.cz/maxamart/apo_semestralka

  Martin Maxa & Michal Lenc, public domain

 *******************************************************************/

#ifndef __UTILS_H__
#define __UTILS_H__

#include <stdlib.h>
#include <termios.h>
#include <unistd.h> // for STDIN_FILENO

typedef struct utilsData
{
    unsigned char *parlcd_base;
    unsigned char *mem_base;
} utilsData_t;

void changeBase(unsigned char *base);
unsigned char *getBase(void); //return base adress of dislay
void changeMemBase(unsigned char *base);
unsigned char *getMemBase(void); //return base adress of mem

void call_termios(int reset);

uint32_t colorRGB(unsigned char r, unsigned char g, unsigned char b);
unsigned short colorRGB565(unsigned char r, unsigned char g, unsigned char b);

void info(const char *str); //info message
void event(const char *str); //event message
void debug(const char *str); //debug message
void error(const char *str); //error message
void warn(const char *str); //warn message

#endif

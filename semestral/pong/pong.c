/*******************************************************************
  Subprogram for pong game

  Git repository: https://gitlab.fel.cvut.cz/maxamart/apo_semestralka

  Martin Maxa & Michal Lenc, public domain

 *******************************************************************/

#include "pong.h"
#include "../mzapo/mzapo_parlcd.h"
#include "../mzapo/mzapo_phys.h"
#include "../mzapo/mzapo_regs.h"
#include "../lcdControl/lcdControl.h"
#include "../utils.h"

static paddle_t paddle[2];
static score_t score;
static ball_t ball;

int pong()
{
    setLCDbackground(0);

    parlcd_write_cmd(getBase(), 0x2c);
    write_free_lines_to_parlcd(49);
    write_words_to_parlcd("P1: W-up S-down", 3);
    write_free_lines_to_parlcd(15);
    write_words_to_parlcd("P2: I-up K-down", 3);
    write_free_lines_to_parlcd(15);
    write_words_to_parlcd("P-pause Q-quit", 3);
    write_free_lines_to_parlcd(49);
    sleep(4);
    //initialize shared data and mutexes
    data_t shared_data;
    shared_data.stop_flag = false;
    shared_data.last_sent = '?';
    shared_data.pause_flag = false;
    shared_data.led = false;
    bool init_error = false;
    if (pthread_mutex_init(&shared_data.mtx, NULL))
    {
        fprintf(stderr, "ERROR: Could not initialize mutex.\n");
        init_error = true;
    }
    if (pthread_cond_init(&shared_data.output_condvar, NULL))
    {
        fprintf(stderr, "ERROR: Could not initialize condvar.\n");
        init_error = true;
    }
    if (init_error)
    {
        return EXIT_FAILURE;
    }
    init_game(); //initialize game

    enum
    {
        INPUT_THREAD,
        CONTROL_THREAD,
        BALL_THREAD,
	    LED_THEAD,
        NUM_THREADS
    };
    const char* thread_names[] = {"Input", "Control", "Ball", "Led"};
    void* (*thd_functions[])(void*) = {
        input_thread,
        control_thread,
        ball_thread,
	    led_thread,
       };
    pthread_t threads[NUM_THREADS]; //array of threads

    for (int i = 0; i < NUM_THREADS; ++i)
    { //start the threads
        int check = pthread_create(&threads[i], NULL, thd_functions[i], &shared_data);
        fprintf(stderr, "INFO: %s thread start: %s\r\n",
                thread_names[i],
                check ? "FAIL" : "ok");
        if (check != 0)
        {
            call_termios(1);
            return EXIT_FAILURE;
        }
    }
    for (int i = 0; i < NUM_THREADS; ++i)
    { //join the threads
        int check = pthread_join(threads[i], NULL);
        fprintf(stderr, "INFO: %s thread joined: %s\r\n",
                thread_names[i],
                check ? "FAIL" : "ok");
    }
    info("Quit pong");
    unsigned c;
    parlcd_write_cmd(getBase(), 0x2c);
    for (int i = 0; i < 320; i++)
    {
        for (int j = 0; j < 480; j++)
        {
            c = 0x0;
            parlcd_write_data(getBase(), c);
        }
    }
    parlcd_write_cmd(getBase(), 0x2c);
    write_free_lines_to_parlcd(10);
    write_words_to_parlcd("Game over", 4);
    write_free_lines_to_parlcd(62);
    if (score.p1 > score.p2) //if player one won
    {
        char result[100];
        write_words_to_parlcd("Player 1 wins", 3);
        write_free_lines_to_parlcd(10);
        sprintf(result, "%s%d", "score ", score.p1);
        write_words_to_parlcd(result, 3); //print result to lcd display
    }
    else if (score.p1 < score.p2) //if player two won
    {
        char result[100];
        write_words_to_parlcd("Player 1 wins", 3);
        write_free_lines_to_parlcd(10);
        sprintf(result, "%s%d", "score ", score.p2);
        write_words_to_parlcd(result, 3); //print result to lcd display
    }
    else //if draw
    {
        write_words_to_parlcd("draw", 3); //print draw
    }
    int line = 1;
    for (int i = 1; i<=32;i++){ //do final led line animation
    	*(volatile uint32_t *)(getMemBase()+SPILED_REG_LED_LINE_o) = line;
	    line *= 2;
	    usleep(10000);
    }
    sleep(2);
    return EXIT_SUCCESS;
}

void init_game()
{
    paddle[0].h = 50;
    paddle[0].w = 10;
    paddle[0].x = 10 + paddle[0].w;
    paddle[0].y = 150;

    paddle[1].h = 50;
    paddle[1].w = 10;
    paddle[1].x = WIDTH_PONG - 12 - paddle[1].w;
    paddle[1].y = 150;

    score.p1 = 0;
    score.p2 = 0;

    ball.x = 200;
    ball.y = 150;
    ball.h = 15;
    ball.w = 15;
    ball.dy = 0;
}

bool paddle_coords(int x, int y)
{
    return ((x <= paddle[0].x && x > paddle[0].x - paddle[0].w && (y >= paddle[0].y && y < paddle[0].y + paddle[0].h)) ||
            (x >= paddle[1].x && x < paddle[1].x + paddle[1].w && (y >= paddle[1].y && y < paddle[1].y + paddle[1].h)));
}

bool ball_coords(int x, int y)
{
    return x <= ball.x && x > ball.x - ball.w && y >= ball.y && y < ball.y + ball.h;
}

void print_board()
{
    parlcd_write_cmd(getBase(), 0x2c);
    write_free_lines_to_parlcd(14);
    char text[100];
    char player1[] = "Player 1";
    char player2[] = "Player 2";
    sprintf(text, "%s       %d       %s       %d",
            player1, score.p1, player2, score.p2);
    write_words_to_parlcd(text, 1);
    write_free_lines_to_parlcd(10);
    for (int i = 0; i < WIDTH_PONG; i++)
    {
        parlcd_write_data(getBase(), 0xffff);
    }
    for (int i = 1; i < HEIGHT_PONG; i++)
    {
        for (int j = 0; j < WIDTH_PONG; j++)
        {
            if (j == 0)
            {
                parlcd_write_data(getBase(), 0xffff);
            }
            else if (j + 1 == WIDTH_PONG)
            {
                parlcd_write_data(getBase(), 0xffff);
            }
            else if (paddle_coords(j, i) || ball_coords(j, i))
            {
                parlcd_write_data(getBase(), 0xffff);
            }
            else
            {
                parlcd_write_data(getBase(), 0x0);
            }
        }
    }
    for (int i = 0; i < WIDTH_PONG; i++)
    {
        parlcd_write_data(getBase(), 0xffff);
    }
}

void *input_thread(void *arg)
{
    data_t *data = (data_t *)arg;
    char input;
    while (data->stop_flag == 0 && (input = getc(stdin)) != EOF)
    {
        pthread_mutex_lock(&data->mtx);
        switch (input) //evaluation of options
        {
        case 'q':
            if (data->pause_flag)
            { //if game is paused
                info("Please unpause the game first");
                break;
            }
            data->stop_flag = 1;
            break;
        case 'p':
            data->pause_flag = !data->pause_flag;
	        if (data->pause_flag)
            {
                info("Pong is paused, press 'p' for continue");
	        }
            else {
                info("Game continues");
            }
            break;
        case 'w':
        case 's':
        case 'i':
        case 'k':
        case 'h':
        case 'A': //arrow up
        case 'B': //arrow down
            data->last_sent = input;
            break;
        default:
            break;
        }
        pthread_cond_broadcast(&data->output_condvar);
        pthread_mutex_unlock(&data->mtx);
    }
    pthread_mutex_lock(&data->mtx);
    data->stop_flag = 1;
    pthread_cond_broadcast(&data->output_condvar);
    pthread_mutex_unlock(&data->mtx);
    return NULL;
}

void *control_thread(void *arg)
{
    data_t *data = (data_t *)arg;
    while (data->stop_flag == 0)
    {
        print_board();
        if (data->pause_flag)
        {
            continue;
        }
        pthread_mutex_lock(&data->mtx);
        switch (data->last_sent)
        {
        case 'q':
            data->stop_flag = 1; //leave the game
            break;
        case 'w':
            paddle[0].y -= 15; //move with paddle[0] up
            if (paddle[0].y <= 0) //if paddle is at the top of game field
            {
                paddle[0].y = 1;
            }
            data->last_sent = '?';
            break;
        case 's':
            paddle[0].y += 15; //move with paddle[0] down
            if (paddle[0].y + paddle[0].h >= HEIGHT_PONG) //if paddle is at the bottom of game field
            {
                paddle[0].y = HEIGHT_PONG - paddle[0].h;
            }
            data->last_sent = '?';
            break;
        case 'A': //arrow up
        case 'i':
            paddle[1].y -= 15;  //move with paddle[1] up
            if (paddle[1].y <= 0) //if paddle is at the top of game field
            {
                paddle[1].y = 1;
            }
            data->last_sent = '?';
            break;
        case 'B': //arrow down
        case 'k':
            paddle[1].y += 15;  //move with paddle[1] down
            if (paddle[1].y + paddle[1].h >= HEIGHT_PONG) //if paddle is at the bottom of game field
            {
                paddle[1].y = HEIGHT_PONG - paddle[1].h;
            }
            data->last_sent = '?';
            break;
        case 'h': //print score to stdout
            printf("Player 1 score: %d\tPlayer 2 score: %d\n", score.p1, score.p2);
            data->last_sent = '?';
            break;
        default:
            break;
        }
        pthread_mutex_unlock(&data->mtx);
    }
    return NULL;
}

void *ball_thread(void *arg)
{
    data_t *data = (data_t *)arg;
    int i = -1;
    while (data->stop_flag == 0)
    {
        if (data->pause_flag) //if paused, then do nothing
        {
            continue;
        }
        if (ball.x - ball.w == paddle[0].x && ball.y + ball.h - 1 >= paddle[0].y && ball.y < (paddle[0].y + paddle[0].h))
        { //if ball hit paddle[0]
            i = i * (-1); //change direction of the ball
            if (ball.y + ball.h - 1 <= paddle[0].y + paddle[0].h / 2)
            {
                ball.dy = -1;
            }
            else
            {
                ball.dy = 1;
            }
        }
        if (ball.x + 1 == paddle[1].x && ball.y + ball.h - 1 >= paddle[1].y && ball.y < (paddle[1].y + paddle[1].h))
        { //if ball hit paddle[1]
            i = i * (-1); //change direction of the ball
            if (ball.y + ball.h - 1 <= paddle[1].y + paddle[1].h / 2)
            {
                ball.dy = -1;
            }
            else
            {
                ball.dy = 1;
            }
        }
        if (ball.y == 1 || ball.y + ball.h == HEIGHT_PONG)
        { //if ball hited top or bottom of game field
            ball.dy = ball.dy * (-1);
        }
        ball.x += i;
        ball.y += ball.dy;
        if (ball.x > WIDTH_PONG || ball.x < 0)
        { //if ball is out of game field
            if (ball.x < 0)
            { //if player 2 scored
                score.p2 += 1;
            }
            else
            { //if player 1 scored
                score.p1 += 1;
            }
            //light up the leds
	        *(volatile uint32_t *)(getMemBase()+SPILED_REG_LED_LINE_o) = pow(2, 32)-1;
	        usleep(50000);
            //turn them off
	        *(volatile uint32_t *)(getMemBase()+SPILED_REG_LED_LINE_o) = 0;
            ball.x = 200;
            ball.y = 150;
            ball.dy = 0;
        }
        usleep(5000);
    }
    return NULL;
}

void *led_thread(void* arg){
	data_t *data = (data_t*)arg;
	int value = 1;
	int counter = 1;
	bool down = false;
	while (data->stop_flag == 0){
        if (data->pause_flag)
        {
            continue;
        }
		if (counter < 32 && !down)
        { //incrrement the led value
			*(volatile uint32_t *)(getMemBase()+SPILED_REG_LED_LINE_o) = value;
			value *=2;
			counter+=1;
			usleep(50000);
			if (counter == 31){
				down = true;
			}
		}
		else if (down)
        { //decrement the led value
			*(volatile uint32_t *)(getMemBase()+SPILED_REG_LED_LINE_o) = value;
			value /= 2;
			counter -=1;
			usleep(50000);
			if (counter == 1){
				down = false;
			}
		}
	}
	return NULL;
}

/*******************************************************************
  Subprogram for pong game

  Git repository: https://gitlab.fel.cvut.cz/maxamart/apo_semestralka

  Martin Maxa & Michal Lenc, public domain

 *******************************************************************/

#ifndef PONG_H
#define PONG_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <termios.h>
#include <unistd.h>  // for STDIN_FILENO
#include <pthread.h>
#include <math.h>

#define WIDTH_PONG           480
#define HEIGHT_PONG          275
#define SCORE_HEIGHT         40

typedef struct paddle {
    int x, y; //position on the screen
    int h, w; //size of the paddle
} paddle_t;

typedef struct ball {
    int x, y; //position on the screen
    int h, w; //size of the ball
    int dy; //change of y coord
} ball_t;

typedef struct score {
    int p1, p2; //score of player 1 and 2
} score_t;

typedef struct data {
    bool pause_flag; //if game is paused
    bool stop_flag; //indicates if the program is running or not;
    bool led;
    char last_sent; //last received letter
    pthread_cond_t output_condvar;
    pthread_mutex_t mtx;
} data_t;

int pong();
void init_game(void); //init score and pos of ball and paddles
bool paddle_coords(int x, int y); //return true if paddle is in x, y coords
bool ball_coords(int x, int y); //return true if ball is in x, y coords
void print_board(); //print pong board to display

void *input_thread(void* arg); //input thread reads chars from stdin
void *control_thread(void* arg); //controling paddles
void *ball_thread(void* arg); //moving with ball
void *led_thread(void* arg); //changing led line value
#endif

echo "Make clean"
make clean
make
clear
echo "Making - done."
echo "Starting copying"
cp font_prop14x16.o fonts
rm font_prop14x16.o
cp font_rom8x16.o fonts
rm font_rom8x16.o
cp game_of_life.o gameOfLife
rm game_of_life.o
cp lcdControl.o lcdControl
rm lcdControl.o
cp image.o image
rm image.o
cp leds.o leds
rm leds.o
cp pong.o pong
rm pong.o
cp mzapo_phys.o mzapo
rm mzapo_phys.o
cp mzapo_parlcd.o mzapo
rm mzapo_parlcd.o
echo "Cleanup done"
echo "Make"
make
echo "====DONE===="

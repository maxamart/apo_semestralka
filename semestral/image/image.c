/*******************************************************************
  Subprogram for applying convolution on pictures and displaying
  them on LCD display

  Git repository: https://gitlab.fel.cvut.cz/maxamart/apo_semestralka

  Martin Maxa & Michal Lenc, public domain

 *******************************************************************/

#include "image.h"
#include "../utils.h"
#include "../lcdControl/lcdControl.h"

/*-----------------Global variables-------------------------------------------*/
unsigned char *imageRGB;

/*-----------------Functions--------------------------------------------------*/
_Bool imageMain(void)
{
	call_termios(1);	 //stop raw mode
	setLCDbackground(0); //white color
	write_free_lines_to_parlcd(120);
	write_words_to_parlcd("Image convolution", 3);
	event("Chose image:");
	info("You can write 'ls'");
	char name[15];
	_Bool w_inp; //wrong input

	do //image input name
	{
		w_inp = false;
		if (scanf("%15s", name) != 1)
		{
			error("Reading input");
		}

		if (!strcmp(name, "ls"))
		{
			info("Reachable images: fractal.ppm, normal.ppm, obraz.ppm");
		}
		else if (!strcmp(name, "q")){
			info("Set quit");
			call_termios(0);
			return true;
		}
		else if (strcmp("fractal.ppm", name) && strcmp("normal.ppm", name) && strcmp("obraz.ppm", name))
		{
			error("Wrong input. Type again. Or type 'q' to quit.");
			w_inp = true;
		}
	} while (!strcmp(name, "ls") || w_inp);
	getchar(); //new line

	info("Reading image");
	if (!readImage(name)) //read image
	{
		error("read image");
		call_termios(0);
		return false;
	}
	else
	{
		info("Reading successful");
	}

	info("Printing..."); //printing
	screenToLCD();
	info("Printing done");

	//convolution loop
	char choice;
	do
	{
		if (!convolutionSetup())
		{
			info("You can write only numbers!");
		}

		call_termios(0);
		info("Pres 'q' for returning to menu, else press any key");

		if (scanf("%c", &choice) != 1)
		{
			call_termios(0);
			return false;
		}

		call_termios(1);
	} while (choice != 'q' && choice != 'Q');

	call_termios(0);
	return true;
}

/*-----------------Functions--------------------------------------------------*/
_Bool readImage(const char *fname)
{
	FILE *ImageFile = fopen(fname, "rb"); //open file with a picture
	if (ImageFile == NULL)
	{
		return false;
	}

	if (fscanf(ImageFile, "P3\n") != 0)
	{ //read P definition
		error("First row reading");
		return false;
	}

	int width, height, max_value;
	int ret;
	ret = fscanf(ImageFile, "%d %d\n%d\n", &width, &height, &max_value); //load height, width and max value

	if (ret != 3)
	{ //check of loaded values
		error("Second and third row reading");
		return false;
	}
	ret = 0;

	unsigned char *imageRGB = malloc(width * height * 3 * sizeof(char)); //allocate mem for height*width*3 (3 = RGB values)

	for (int i = 0; !feof(ImageFile); ++i)
	{
		if (fscanf(ImageFile, "%hhd", (imageRGB + i)) != 1)
		{
			if (!feof(ImageFile))
			{
				error("reading file values");
			}
		}
	}

	int k = 0;
	for (int i = 0; i < ROWS; ++i)
	{
		for (int j = 0; j < COLS; ++j)
		{
			screen[i * COLS + j] = colorRGB565(imageRGB[k], imageRGB[k + 1], imageRGB[k + 2]);
			k += 3;
		}
	}

	fclose(ImageFile); //close file
	free(imageRGB);	   //free memory

	return true;
}

/*-----------------Functions--------------------------------------------------*/
_Bool convolutionSetup(void)
{
	event("Change convoluton matrix value");
	event("Write 9 values of matrix 'a11 a12 a13 a21 ... a33'");
	write_words_to_parlcd("Convolution matrix setup...", 2);

	int matrix[9] = {0};

	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			printf("Write value for matrix[%d][%d] = \t", i, j);
			if (scanf("%d", (matrix + i * 3 + j)) != 1)
			{
				error("Reading value");
				getchar(); //reading newline
				return false;
			}
		}
	}
	getchar(); //reading newline

	//convolution
	return convolution(matrix[0], matrix[1], matrix[2], matrix[3], matrix[4],
					   matrix[5], matrix[6], matrix[7], matrix[8]);
}

/*-----------------Functions--------------------------------------------------*/
_Bool convolution(int a11, int a12, int a13, int a21, int a22, int a23, int a31, int a32, int a33)
{
	unsigned short imageOut[ROWS * COLS];

	for (int j = 0; j < COLS; ++j)
	{ //copy first value for first line
		imageOut[j] = screen[j];
	}

	for (int i = 1; i < ROWS - 1; ++i)
	{										   //main cykle of convolution, going throught lines
		imageOut[i * COLS] = screen[i * COLS]; //copy first collumn
		for (int j = 1; j < COLS - 1; ++j)
		{ //going throught collumns
			unsigned short newcol1 = a11 * screen[(i - 1) * COLS + j - 1] + a12 * screen[(i - 1) * COLS + j] + a13 * screen[(i - 1) * COLS + j + 1];
			unsigned short newcol2 = a21 * screen[i * COLS + j - 1] + a22 * screen[i * COLS + j] + a23 * screen[i * COLS + j + 1];
			unsigned short newcol3 = a31 * screen[(i + 1) * COLS + j - 1] + a32 * screen[(i + 1) * COLS + j] + a33 * screen[(i + 1) * COLS + j + 1];
			unsigned short newcol = newcol1 + newcol2 + newcol3;
			if (newcol < 0)
			{
				newcol = 0;
			}
			else if (newcol > 65535)
			{
				newcol = 65535;
			}

			imageOut[i * COLS + j] = newcol;
		}

		imageOut[(i + 1) * COLS - 1] = screen[(i + 1) * COLS - 1];
	}

	for (int j = 0; j < COLS; ++j)
	{ //copy values for last line
		imageOut[(ROWS - 1) * COLS + j] = screen[(ROWS - 1) * COLS + j];
	}

	parlcd_write_cmd(getBase(), 0x2c); //printing image
	for (int i = 0; i < ROWS * COLS; ++i)
	{
		parlcd_write_data(getBase(), imageOut[i]);
	}

	return true;
}

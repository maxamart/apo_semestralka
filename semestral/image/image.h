/*******************************************************************
  Subprogram for applying convolution on pictures and displaying
  them on LCD display

  Git repository: https://gitlab.fel.cvut.cz/maxamart/apo_semestralka

  Martin Maxa & Michal Lenc, public domain

 *******************************************************************/

#ifndef IMAGE_TO_LED
#define IMAGE_TO_LED

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "../mzapo/mzapo_parlcd.h"
#include "../mzapo/mzapo_phys.h"
#include "../mzapo/mzapo_regs.h"


#define ROWS 320
#define COLS 480

extern unsigned short screen[COLS * ROWS];
_Bool imageMain(void); //main fuction for image subprogram

_Bool readImage(const char *fname); // reads image from file
_Bool convolutionSetup(void); //setting convolution parameters
_Bool convolution(int a11, int a12, int a13, int a21, int a22, int a23, int a31, int a32, int a33); //computing convolution


#endif

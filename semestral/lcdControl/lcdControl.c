/*******************************************************************
  Subprogram for printing fonts and graphics on LCD display

  Git repository: https://gitlab.fel.cvut.cz/maxamart/apo_semestralka

  Martin Maxa & Michal Lenc, public domain

 *******************************************************************/

#include "../mzapo/mzapo_parlcd.h"
#include "../mzapo/mzapo_phys.h"
#include "../mzapo/mzapo_regs.h"
#include "../fonts/font_types.h"
#include "../utils.h"
#include "lcdControl.h"

#define BOX_80x240 19200

void screenPrintLCD(unsigned short *frameBuffer)
{
	parlcd_write_cmd(getBase(), 0x2c); //printing the image

	for (int i = 0; i < ROWS * COLS; ++i)
	{
		parlcd_write_data(getBase(), frameBuffer[i]);
	}
}

void screenToLCD()
{
	parlcd_write_cmd(getBase(), 0x2c);

	for (int i = 0; i < ROWS * COLS; ++i)
	{
		parlcd_write_data(getBase(), screen[i]);
	}
}

void setLCDbackground(const unsigned short color)
{
	for (int i = 0; i < ROWS * COLS; ++i)
	{
		screen[i] = color;
	}
	screenToLCD();
}

void setScreenBackground(const unsigned short color)
{
	for (int i = 0; i < ROWS * COLS; ++i)
	{
		screen[i] = color;
	}
}

_Bool create_menu(void)
{
	setLCDbackground(0);

	for (int i = 1; i < 3; ++i)
	{
		for (int j = 0; j < 2; ++j)
		{
			create_box(i, j, 1296);
		}
	}

	create_box(3, 1, 5000);

	write_string("MENU", 3, 0, 0);

	write_string("I - Image", 1, 1, 0);
	write_string("G - Game of Life", 1, 1, 1);

	write_string("L - LEDs", 1, 2, 0);
	write_string("P - PONG", 1, 2, 1);

	write_string("Q - QUIT", 1, 3, 1);

	screenToLCD();

	return true;
}

void create_box(int row, int col, unsigned short color)
{
	if (row > 4 || col > 1)
	{
		error("Wrong input");
	}

	int start_i = 80 * row;
	int start_j = col * COLS / 2;
	int end_i = 80 * (row + 1);
	int end_j = (col + 1) * COLS / 2;

	for (int i = start_i; i < end_i; ++i)
	{
		for (int j = start_j; j < end_j; ++j)
		{
			if (j < start_j + 2 || i < start_i + 2 || j > end_j - 2 || i > end_i - 2) //border
			{
				screen[i * COLS + j] = 65535; //white border
			}
			else
			{
				screen[i * COLS + j] = color; //background color
			}
		}
	}
}

void write_string(char words[], int zoom, int row, int col)
{
	int set;
	int words_len = strlen(words);
	int tmp;
	int pos = 0;
	int offset = 2 * row * BOX_80x240 + COLS * col / 2;
	offset += 40 + 30 * COLS; //padding

	for (int j = 0; j < words_len; j++)
	{
		tmp = (int)words[j];
		pos = 0;
		for (int i = 0; i < zoom * 16; i++)
		{
			if (tmp >= 'a' && tmp <= 'z')
			{
				tmp -= 32; //convert small letters to big letters
			}
			font_bits_t bitmap = font_rom8x16.bits[tmp * 16 + pos] >> 8;
			int o = 0;
			for (int k = zoom * (j + 1) * 8; k > zoom * j * 8; k--)
			{							 //for every bit
				set = (bitmap >> o) & 1; //check if bit is one or zero
				o++;
				if (set == 1)
				{
					for (int z = 0; z < zoom; z++)
					{
						screen[(i * COLS) + k + offset] = 0xffff;
					}
				}
			}
			if (i != 0 && i % zoom == 0)
			{
				pos++; //move pos each font_zoom cykles
			}
		}
	}
}

/*-----------------Functions--------------------------------------------------*/
void write_char_to_parlcd(int character)
{
	unsigned c;
	int set;
	for (int i = 0; i < font_rom8x16.height; i++)
	{
		font_bits_t bitmap = font_rom8x16.bits[character + i] >> 8;
		for (int j = 1; j <= font_rom8x16.maxwidth; j++)
		{
			set = (bitmap >> j) & 1;
			if (set == 1)
			{
				c = 0xffff;
			}
			else
			{
				c = 0x0;
			}
			parlcd_write_data(getBase(), c);
		} //end for
		for (int j = 8; j < 480; j++)
		{
			c = 0x0;
			parlcd_write_data(getBase(), c);
		}
	} //end i for
}

//fuction writes sequence of words to lcd
void write_words_to_parlcd(char words[], int font_zoom)
{
	unsigned c;
	int set;
	int pos = 0;
	int words_len = strlen(words);
	int start = (COLS / 2) - ((words_len * (font_zoom * font_rom8x16.maxwidth + 1)) / 2);
	for (int i = 0; i < font_zoom * font_rom8x16.height; i++)
	{
		for (int j = 0; j < start; j++)
		{ //get to the middle
			parlcd_write_data(getBase(), 0x0);
		}
		for (int j = 0; j < words_len; j++)
		{
			int tmp = (int)words[j]; //convert char to int decimal
			if (tmp >= 97 && tmp <= 122)
			{
				tmp -= 32; //convert small letters to big letters
			}
			font_bits_t bitmap = font_rom8x16.bits[tmp * font_rom8x16.height + pos] >> 8; //convert 16 hexa to 8 hexa
			for (int k = 8; k >= 1; k--)
			{							 //for every bit
				set = (bitmap >> k) & 1; //check if bit is one or zero
				if (set == 1)
				{
					c = 0xffff;
				}
				else
				{
					c = 0x0;
				}
				for (int z = 0; z < font_zoom; z++)
				{ //write to parlcd font_zoom times
					parlcd_write_data(getBase(), c);
				}
			}
			parlcd_write_data(getBase(), 0x0); //one free pixel
		}
		if (i != 0 && i % font_zoom == 0)
		{
			pos++; //move pos each font_zoom cykles
		}
		for (int j = start + words_len * (font_zoom * font_rom8x16.maxwidth + 1); j < COLS; j++)
		{ //go to end of the line
			parlcd_write_data(getBase(), 0x0);
		}
	}
}

//func write free lines to display
void write_free_lines_to_parlcd(int number_of_lines)
{
	for (int i = 0; i < number_of_lines; i++)
	{
		for (int j = 0; j < 480; j++)
		{
			parlcd_write_data(getBase(), 0x0);
		}
	}
}

void write_menu_to_parlcd()
{
	char g[] = "G - Game of Life";
	char l[] = "L - LEDS";
	char i[] = "I - IMAGE";
	char p[] = "P - PONG";
	char q[] = "Q - QUIT";
	parlcd_write_cmd(getBase(), 0x2c);
	write_free_lines_to_parlcd(10);
	write_words_to_parlcd(g, 3);
	write_free_lines_to_parlcd(15);
	write_words_to_parlcd(l, 3);
	write_free_lines_to_parlcd(15);
	write_words_to_parlcd(i, 3);
	write_free_lines_to_parlcd(15);
	write_words_to_parlcd(p, 3);
	write_free_lines_to_parlcd(15);
	write_words_to_parlcd(q, 3);
	write_free_lines_to_parlcd(10);
}

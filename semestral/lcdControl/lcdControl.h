/*******************************************************************
  Subprogram for printing fonts and graphics on LCD display

  Git repository: https://gitlab.fel.cvut.cz/maxamart/apo_semestralka

  Martin Maxa & Michal Lenc, public domain

 *******************************************************************/

#ifndef _lcdeControl_
#define _lcdControl_

#include <stdlib.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define COLS 480
#define ROWS 320

unsigned short screen[ROWS * COLS];

void screenPrintLCD(unsigned short *frameBuffer); //printing the image
void screenToLCD(); //get screen to LCD display
void setLCDbackground(const unsigned short color); //set LCD backround
void setScreenBackground(const unsigned short color); //set screen colour
_Bool create_menu(void); //create graphic menu
void create_box(int row, int col, unsigned short color); //create box for menu
void write_string(char words[], int zoom, int row, int col); //write string for graphic menu

void write_char_to_parlcd(int character); //write one character to lcd
void write_words_to_parlcd(char words[], int font_zoom); //write sequence of chars to lcd
void write_free_lines_to_parlcd(int number_of_lines); //write free lines to lcd
void write_menu_to_parlcd(void); //write text menu to lcd


#endif
